﻿
namespace RevealsGenomicsNetCore
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBoxExcelPath = new MetroSet_UI.Controls.MetroSetTextBox();
            this.textBoxRccPath = new MetroSet_UI.Controls.MetroSetTextBox();
            this.buttonLoadRccFile = new MetroSet_UI.Controls.MetroSetButton();
            this.buttonLoadExcel = new MetroSet_UI.Controls.MetroSetButton();
            this.richTextBox1 = new MetroSet_UI.Controls.MetroSetRichTextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.metroSetPanel1 = new MetroSet_UI.Controls.MetroSetPanel();
            this.metroSetLabel5 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetButton1 = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetTextBoxPath = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetLabel4 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetLabel3 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetLabel2 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetLabel1 = new MetroSet_UI.Controls.MetroSetLabel();
            this.metroSetTextBox2 = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetTextBox1 = new MetroSet_UI.Controls.MetroSetTextBox();
            this.metroSetButtonPDF = new MetroSet_UI.Controls.MetroSetButton();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroSetButtonClose = new MetroSet_UI.Controls.MetroSetButton();
            this.metroSetTabControl1 = new MetroSet_UI.Controls.MetroSetTabControl();
            this.tabPageQC3 = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.tabPageQC2 = new System.Windows.Forms.TabPage();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.tabPageQC1 = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tabPageCodes = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.tabPageRCC = new System.Windows.Forms.TabPage();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.metroSetPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.metroSetTabControl1.SuspendLayout();
            this.tabPageQC3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.tabPageQC2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            this.tabPageQC1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.tabPageCodes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.tabPageRCC.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBoxExcelPath
            // 
            this.textBoxExcelPath.AutoCompleteCustomSource = null;
            this.textBoxExcelPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.textBoxExcelPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.textBoxExcelPath.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.textBoxExcelPath.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.textBoxExcelPath.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.textBoxExcelPath.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.textBoxExcelPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxExcelPath.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.textBoxExcelPath.Image = null;
            this.textBoxExcelPath.IsDerivedStyle = true;
            this.textBoxExcelPath.Lines = null;
            this.textBoxExcelPath.Location = new System.Drawing.Point(115, 50);
            this.textBoxExcelPath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxExcelPath.MaxLength = 32767;
            this.textBoxExcelPath.Multiline = false;
            this.textBoxExcelPath.Name = "textBoxExcelPath";
            this.textBoxExcelPath.ReadOnly = false;
            this.textBoxExcelPath.Size = new System.Drawing.Size(758, 27);
            this.textBoxExcelPath.Style = MetroSet_UI.Enums.Style.Light;
            this.textBoxExcelPath.StyleManager = null;
            this.textBoxExcelPath.TabIndex = 0;
            this.textBoxExcelPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.textBoxExcelPath.ThemeAuthor = "Narwin";
            this.textBoxExcelPath.ThemeName = "MetroLite";
            this.textBoxExcelPath.UseSystemPasswordChar = false;
            this.textBoxExcelPath.WatermarkText = "";
            // 
            // textBoxRccPath
            // 
            this.textBoxRccPath.AutoCompleteCustomSource = null;
            this.textBoxRccPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.textBoxRccPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.textBoxRccPath.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.textBoxRccPath.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.textBoxRccPath.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.textBoxRccPath.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.textBoxRccPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.textBoxRccPath.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.textBoxRccPath.Image = null;
            this.textBoxRccPath.IsDerivedStyle = true;
            this.textBoxRccPath.Lines = null;
            this.textBoxRccPath.Location = new System.Drawing.Point(115, 87);
            this.textBoxRccPath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.textBoxRccPath.MaxLength = 32767;
            this.textBoxRccPath.Multiline = false;
            this.textBoxRccPath.Name = "textBoxRccPath";
            this.textBoxRccPath.ReadOnly = false;
            this.textBoxRccPath.Size = new System.Drawing.Size(758, 27);
            this.textBoxRccPath.Style = MetroSet_UI.Enums.Style.Light;
            this.textBoxRccPath.StyleManager = null;
            this.textBoxRccPath.TabIndex = 1;
            this.textBoxRccPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.textBoxRccPath.ThemeAuthor = "Narwin";
            this.textBoxRccPath.ThemeName = "MetroLite";
            this.textBoxRccPath.UseSystemPasswordChar = false;
            this.textBoxRccPath.WatermarkText = "";
            // 
            // buttonLoadRccFile
            // 
            this.buttonLoadRccFile.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadRccFile.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadRccFile.DisabledForeColor = System.Drawing.Color.Gray;
            this.buttonLoadRccFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonLoadRccFile.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.buttonLoadRccFile.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.buttonLoadRccFile.HoverTextColor = System.Drawing.Color.White;
            this.buttonLoadRccFile.IsDerivedStyle = true;
            this.buttonLoadRccFile.Location = new System.Drawing.Point(897, 88);
            this.buttonLoadRccFile.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoadRccFile.Name = "buttonLoadRccFile";
            this.buttonLoadRccFile.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadRccFile.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadRccFile.NormalTextColor = System.Drawing.Color.White;
            this.buttonLoadRccFile.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.buttonLoadRccFile.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.buttonLoadRccFile.PressTextColor = System.Drawing.Color.White;
            this.buttonLoadRccFile.Size = new System.Drawing.Size(144, 30);
            this.buttonLoadRccFile.Style = MetroSet_UI.Enums.Style.Light;
            this.buttonLoadRccFile.StyleManager = null;
            this.buttonLoadRccFile.TabIndex = 3;
            this.buttonLoadRccFile.Text = "Select RCC Files";
            this.buttonLoadRccFile.ThemeAuthor = "Narwin";
            this.buttonLoadRccFile.ThemeName = "MetroLite";
            this.buttonLoadRccFile.Click += new System.EventHandler(this.buttonLoadRccFile_Click);
            // 
            // buttonLoadExcel
            // 
            this.buttonLoadExcel.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadExcel.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadExcel.DisabledForeColor = System.Drawing.Color.Gray;
            this.buttonLoadExcel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.buttonLoadExcel.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.buttonLoadExcel.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.buttonLoadExcel.HoverTextColor = System.Drawing.Color.White;
            this.buttonLoadExcel.IsDerivedStyle = true;
            this.buttonLoadExcel.Location = new System.Drawing.Point(897, 50);
            this.buttonLoadExcel.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.buttonLoadExcel.Name = "buttonLoadExcel";
            this.buttonLoadExcel.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadExcel.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.buttonLoadExcel.NormalTextColor = System.Drawing.Color.White;
            this.buttonLoadExcel.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.buttonLoadExcel.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.buttonLoadExcel.PressTextColor = System.Drawing.Color.White;
            this.buttonLoadExcel.Size = new System.Drawing.Size(144, 30);
            this.buttonLoadExcel.Style = MetroSet_UI.Enums.Style.Light;
            this.buttonLoadExcel.StyleManager = null;
            this.buttonLoadExcel.TabIndex = 2;
            this.buttonLoadExcel.Text = "Select TXT File";
            this.buttonLoadExcel.ThemeAuthor = "Narwin";
            this.buttonLoadExcel.ThemeName = "MetroLite";
            this.buttonLoadExcel.Click += new System.EventHandler(this.buttonLoadExcel_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.AutoWordSelection = false;
            this.richTextBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.richTextBox1.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.richTextBox1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.richTextBox1.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.richTextBox1.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.richTextBox1.IsDerivedStyle = true;
            this.richTextBox1.Lines = null;
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.richTextBox1.MaxLength = 32767;
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(1068, 359);
            this.richTextBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.richTextBox1.StyleManager = null;
            this.richTextBox1.TabIndex = 3;
            this.richTextBox1.ThemeAuthor = "Narwin";
            this.richTextBox1.ThemeName = "MetroLite";
            this.richTextBox1.WordWrap = true;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Multiselect = true;
            // 
            // metroSetPanel1
            // 
            this.metroSetPanel1.BackgroundColor = System.Drawing.Color.White;
            this.metroSetPanel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(150)))), ((int)(((byte)(150)))), ((int)(((byte)(150)))));
            this.metroSetPanel1.BorderThickness = 1;
            this.metroSetPanel1.Controls.Add(this.metroSetLabel5);
            this.metroSetPanel1.Controls.Add(this.metroSetButton1);
            this.metroSetPanel1.Controls.Add(this.metroSetTextBoxPath);
            this.metroSetPanel1.Controls.Add(this.metroSetLabel4);
            this.metroSetPanel1.Controls.Add(this.metroSetLabel3);
            this.metroSetPanel1.Controls.Add(this.metroSetLabel2);
            this.metroSetPanel1.Controls.Add(this.metroSetLabel1);
            this.metroSetPanel1.Controls.Add(this.metroSetTextBox2);
            this.metroSetPanel1.Controls.Add(this.metroSetTextBox1);
            this.metroSetPanel1.Controls.Add(this.metroSetButtonPDF);
            this.metroSetPanel1.Controls.Add(this.buttonLoadRccFile);
            this.metroSetPanel1.Controls.Add(this.buttonLoadExcel);
            this.metroSetPanel1.Controls.Add(this.textBoxRccPath);
            this.metroSetPanel1.Controls.Add(this.textBoxExcelPath);
            this.metroSetPanel1.IsDerivedStyle = true;
            this.metroSetPanel1.Location = new System.Drawing.Point(12, 141);
            this.metroSetPanel1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetPanel1.Name = "metroSetPanel1";
            this.metroSetPanel1.Size = new System.Drawing.Size(1072, 203);
            this.metroSetPanel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetPanel1.StyleManager = null;
            this.metroSetPanel1.TabIndex = 4;
            this.metroSetPanel1.ThemeAuthor = "Narwin";
            this.metroSetPanel1.ThemeName = "MetroLite";
            // 
            // metroSetLabel5
            // 
            this.metroSetLabel5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetLabel5.IsDerivedStyle = true;
            this.metroSetLabel5.Location = new System.Drawing.Point(29, 18);
            this.metroSetLabel5.Name = "metroSetLabel5";
            this.metroSetLabel5.Size = new System.Drawing.Size(70, 23);
            this.metroSetLabel5.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel5.StyleManager = null;
            this.metroSetLabel5.TabIndex = 13;
            this.metroSetLabel5.Text = "Path:";
            this.metroSetLabel5.ThemeAuthor = "Narwin";
            this.metroSetLabel5.ThemeName = "MetroLite";
            // 
            // metroSetButton1
            // 
            this.metroSetButton1.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButton1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButton1.DisabledForeColor = System.Drawing.Color.Gray;
            this.metroSetButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetButton1.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.metroSetButton1.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.metroSetButton1.HoverTextColor = System.Drawing.Color.White;
            this.metroSetButton1.IsDerivedStyle = true;
            this.metroSetButton1.Location = new System.Drawing.Point(897, 14);
            this.metroSetButton1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetButton1.Name = "metroSetButton1";
            this.metroSetButton1.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButton1.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButton1.NormalTextColor = System.Drawing.Color.White;
            this.metroSetButton1.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.metroSetButton1.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.metroSetButton1.PressTextColor = System.Drawing.Color.White;
            this.metroSetButton1.Size = new System.Drawing.Size(144, 30);
            this.metroSetButton1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetButton1.StyleManager = null;
            this.metroSetButton1.TabIndex = 12;
            this.metroSetButton1.Text = "Select Working Path";
            this.metroSetButton1.ThemeAuthor = "Narwin";
            this.metroSetButton1.ThemeName = "MetroLite";
            this.metroSetButton1.Click += new System.EventHandler(this.metroSetButton1_Click);
            // 
            // metroSetTextBoxPath
            // 
            this.metroSetTextBoxPath.AutoCompleteCustomSource = null;
            this.metroSetTextBoxPath.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.metroSetTextBoxPath.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.metroSetTextBoxPath.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetTextBoxPath.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.metroSetTextBoxPath.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetTextBoxPath.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.metroSetTextBoxPath.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetTextBoxPath.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.metroSetTextBoxPath.Image = null;
            this.metroSetTextBoxPath.IsDerivedStyle = true;
            this.metroSetTextBoxPath.Lines = null;
            this.metroSetTextBoxPath.Location = new System.Drawing.Point(115, 14);
            this.metroSetTextBoxPath.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetTextBoxPath.MaxLength = 32767;
            this.metroSetTextBoxPath.Multiline = false;
            this.metroSetTextBoxPath.Name = "metroSetTextBoxPath";
            this.metroSetTextBoxPath.ReadOnly = false;
            this.metroSetTextBoxPath.Size = new System.Drawing.Size(758, 27);
            this.metroSetTextBoxPath.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetTextBoxPath.StyleManager = null;
            this.metroSetTextBoxPath.TabIndex = 11;
            this.metroSetTextBoxPath.Text = "D:\\SCRIPT\\HER2DX_FinalList.txt";
            this.metroSetTextBoxPath.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.metroSetTextBoxPath.ThemeAuthor = "Narwin";
            this.metroSetTextBoxPath.ThemeName = "MetroLite";
            this.metroSetTextBoxPath.UseSystemPasswordChar = false;
            this.metroSetTextBoxPath.WatermarkText = "";
            // 
            // metroSetLabel4
            // 
            this.metroSetLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetLabel4.IsDerivedStyle = true;
            this.metroSetLabel4.Location = new System.Drawing.Point(29, 161);
            this.metroSetLabel4.Name = "metroSetLabel4";
            this.metroSetLabel4.Size = new System.Drawing.Size(80, 23);
            this.metroSetLabel4.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel4.StyleManager = null;
            this.metroSetLabel4.TabIndex = 10;
            this.metroSetLabel4.Text = "BT474 (2):";
            this.metroSetLabel4.ThemeAuthor = "Narwin";
            this.metroSetLabel4.ThemeName = "MetroLite";
            // 
            // metroSetLabel3
            // 
            this.metroSetLabel3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetLabel3.IsDerivedStyle = true;
            this.metroSetLabel3.Location = new System.Drawing.Point(29, 126);
            this.metroSetLabel3.Name = "metroSetLabel3";
            this.metroSetLabel3.Size = new System.Drawing.Size(80, 23);
            this.metroSetLabel3.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel3.StyleManager = null;
            this.metroSetLabel3.TabIndex = 9;
            this.metroSetLabel3.Text = "BT474 (1):";
            this.metroSetLabel3.ThemeAuthor = "Narwin";
            this.metroSetLabel3.ThemeName = "MetroLite";
            // 
            // metroSetLabel2
            // 
            this.metroSetLabel2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetLabel2.IsDerivedStyle = true;
            this.metroSetLabel2.Location = new System.Drawing.Point(29, 91);
            this.metroSetLabel2.Name = "metroSetLabel2";
            this.metroSetLabel2.Size = new System.Drawing.Size(70, 23);
            this.metroSetLabel2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel2.StyleManager = null;
            this.metroSetLabel2.TabIndex = 8;
            this.metroSetLabel2.Text = "RCC File:";
            this.metroSetLabel2.ThemeAuthor = "Narwin";
            this.metroSetLabel2.ThemeName = "MetroLite";
            // 
            // metroSetLabel1
            // 
            this.metroSetLabel1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetLabel1.IsDerivedStyle = true;
            this.metroSetLabel1.Location = new System.Drawing.Point(29, 54);
            this.metroSetLabel1.Name = "metroSetLabel1";
            this.metroSetLabel1.Size = new System.Drawing.Size(70, 23);
            this.metroSetLabel1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetLabel1.StyleManager = null;
            this.metroSetLabel1.TabIndex = 7;
            this.metroSetLabel1.Text = "TXT File:";
            this.metroSetLabel1.ThemeAuthor = "Narwin";
            this.metroSetLabel1.ThemeName = "MetroLite";
            // 
            // metroSetTextBox2
            // 
            this.metroSetTextBox2.AutoCompleteCustomSource = null;
            this.metroSetTextBox2.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.metroSetTextBox2.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.metroSetTextBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetTextBox2.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.metroSetTextBox2.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetTextBox2.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.metroSetTextBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetTextBox2.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.metroSetTextBox2.Image = null;
            this.metroSetTextBox2.IsDerivedStyle = true;
            this.metroSetTextBox2.Lines = null;
            this.metroSetTextBox2.Location = new System.Drawing.Point(115, 157);
            this.metroSetTextBox2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetTextBox2.MaxLength = 32767;
            this.metroSetTextBox2.Multiline = false;
            this.metroSetTextBox2.Name = "metroSetTextBox2";
            this.metroSetTextBox2.ReadOnly = false;
            this.metroSetTextBox2.Size = new System.Drawing.Size(758, 27);
            this.metroSetTextBox2.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetTextBox2.StyleManager = null;
            this.metroSetTextBox2.TabIndex = 6;
            this.metroSetTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.metroSetTextBox2.ThemeAuthor = "Narwin";
            this.metroSetTextBox2.ThemeName = "MetroLite";
            this.metroSetTextBox2.UseSystemPasswordChar = false;
            this.metroSetTextBox2.WatermarkText = "";
            // 
            // metroSetTextBox1
            // 
            this.metroSetTextBox1.AutoCompleteCustomSource = null;
            this.metroSetTextBox1.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.None;
            this.metroSetTextBox1.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.None;
            this.metroSetTextBox1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetTextBox1.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(204)))), ((int)(((byte)(204)))));
            this.metroSetTextBox1.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(155)))), ((int)(((byte)(155)))), ((int)(((byte)(155)))));
            this.metroSetTextBox1.DisabledForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(136)))), ((int)(((byte)(136)))), ((int)(((byte)(136)))));
            this.metroSetTextBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetTextBox1.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(102)))), ((int)(((byte)(102)))), ((int)(((byte)(102)))));
            this.metroSetTextBox1.Image = null;
            this.metroSetTextBox1.IsDerivedStyle = true;
            this.metroSetTextBox1.Lines = null;
            this.metroSetTextBox1.Location = new System.Drawing.Point(115, 122);
            this.metroSetTextBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetTextBox1.MaxLength = 32767;
            this.metroSetTextBox1.Multiline = false;
            this.metroSetTextBox1.Name = "metroSetTextBox1";
            this.metroSetTextBox1.ReadOnly = false;
            this.metroSetTextBox1.Size = new System.Drawing.Size(758, 27);
            this.metroSetTextBox1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetTextBox1.StyleManager = null;
            this.metroSetTextBox1.TabIndex = 5;
            this.metroSetTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            this.metroSetTextBox1.ThemeAuthor = "Narwin";
            this.metroSetTextBox1.ThemeName = "MetroLite";
            this.metroSetTextBox1.UseSystemPasswordChar = false;
            this.metroSetTextBox1.WatermarkText = "";
            // 
            // metroSetButtonPDF
            // 
            this.metroSetButtonPDF.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonPDF.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonPDF.DisabledForeColor = System.Drawing.Color.Gray;
            this.metroSetButtonPDF.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetButtonPDF.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.metroSetButtonPDF.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.metroSetButtonPDF.HoverTextColor = System.Drawing.Color.White;
            this.metroSetButtonPDF.IsDerivedStyle = true;
            this.metroSetButtonPDF.Location = new System.Drawing.Point(897, 126);
            this.metroSetButtonPDF.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetButtonPDF.Name = "metroSetButtonPDF";
            this.metroSetButtonPDF.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonPDF.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonPDF.NormalTextColor = System.Drawing.Color.White;
            this.metroSetButtonPDF.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.metroSetButtonPDF.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.metroSetButtonPDF.PressTextColor = System.Drawing.Color.White;
            this.metroSetButtonPDF.Size = new System.Drawing.Size(144, 58);
            this.metroSetButtonPDF.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetButtonPDF.StyleManager = null;
            this.metroSetButtonPDF.TabIndex = 4;
            this.metroSetButtonPDF.Text = "Generate PDF";
            this.metroSetButtonPDF.ThemeAuthor = "Narwin";
            this.metroSetButtonPDF.ThemeName = "MetroLite";
            this.metroSetButtonPDF.Click += new System.EventHandler(this.metroSetButtonPDF_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.Window;
            this.pictureBox1.BackgroundImage = global::RevealsGenomicsNetCore.Properties.Resources.Logo2;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.pictureBox1.Location = new System.Drawing.Point(12, 70);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(1075, 63);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // metroSetButtonClose
            // 
            this.metroSetButtonClose.DisabledBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonClose.DisabledBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(120)))), ((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonClose.DisabledForeColor = System.Drawing.Color.Gray;
            this.metroSetButtonClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.metroSetButtonClose.HoverBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.metroSetButtonClose.HoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(207)))), ((int)(((byte)(255)))));
            this.metroSetButtonClose.HoverTextColor = System.Drawing.Color.White;
            this.metroSetButtonClose.IsDerivedStyle = true;
            this.metroSetButtonClose.Location = new System.Drawing.Point(1054, 12);
            this.metroSetButtonClose.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.metroSetButtonClose.Name = "metroSetButtonClose";
            this.metroSetButtonClose.NormalBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonClose.NormalColor = System.Drawing.Color.FromArgb(((int)(((byte)(65)))), ((int)(((byte)(177)))), ((int)(((byte)(225)))));
            this.metroSetButtonClose.NormalTextColor = System.Drawing.Color.White;
            this.metroSetButtonClose.PressBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.metroSetButtonClose.PressColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(147)))), ((int)(((byte)(195)))));
            this.metroSetButtonClose.PressTextColor = System.Drawing.Color.White;
            this.metroSetButtonClose.Size = new System.Drawing.Size(34, 27);
            this.metroSetButtonClose.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetButtonClose.StyleManager = null;
            this.metroSetButtonClose.TabIndex = 6;
            this.metroSetButtonClose.Text = "X";
            this.metroSetButtonClose.ThemeAuthor = "Narwin";
            this.metroSetButtonClose.ThemeName = "MetroLite";
            this.metroSetButtonClose.Click += new System.EventHandler(this.metroSetButtonClose_Click);
            // 
            // metroSetTabControl1
            // 
            this.metroSetTabControl1.AnimateEasingType = MetroSet_UI.Enums.EasingType.CubeOut;
            this.metroSetTabControl1.AnimateTime = 200;
            this.metroSetTabControl1.BackgroundColor = System.Drawing.Color.White;
            this.metroSetTabControl1.Controls.Add(this.tabPageQC3);
            this.metroSetTabControl1.Controls.Add(this.tabPageQC2);
            this.metroSetTabControl1.Controls.Add(this.tabPageQC1);
            this.metroSetTabControl1.Controls.Add(this.tabPageCodes);
            this.metroSetTabControl1.Controls.Add(this.tabPageRCC);
            this.metroSetTabControl1.IsDerivedStyle = true;
            this.metroSetTabControl1.ItemSize = new System.Drawing.Size(100, 38);
            this.metroSetTabControl1.Location = new System.Drawing.Point(12, 351);
            this.metroSetTabControl1.Name = "metroSetTabControl1";
            this.metroSetTabControl1.SelectedIndex = 0;
            this.metroSetTabControl1.SelectedTextColor = System.Drawing.Color.White;
            this.metroSetTabControl1.Size = new System.Drawing.Size(1076, 405);
            this.metroSetTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.metroSetTabControl1.Speed = 100;
            this.metroSetTabControl1.Style = MetroSet_UI.Enums.Style.Light;
            this.metroSetTabControl1.StyleManager = null;
            this.metroSetTabControl1.TabIndex = 7;
            this.metroSetTabControl1.TabStyle = MetroSet_UI.Enums.TabStyle.Style2;
            this.metroSetTabControl1.ThemeAuthor = "Btw";
            this.metroSetTabControl1.ThemeName = "Btw";
            this.metroSetTabControl1.UnselectedTextColor = System.Drawing.Color.Gray;
            this.metroSetTabControl1.UseAnimation = false;
            this.metroSetTabControl1.SelectedIndexChanged += new System.EventHandler(this.metroSetTabControl1_SelectedIndexChanged);
            // 
            // tabPageQC3
            // 
            this.tabPageQC3.Controls.Add(this.dataGridView4);
            this.tabPageQC3.Location = new System.Drawing.Point(4, 42);
            this.tabPageQC3.Name = "tabPageQC3";
            this.tabPageQC3.Size = new System.Drawing.Size(1068, 359);
            this.tabPageQC3.TabIndex = 4;
            this.tabPageQC3.Text = "QC3";
            // 
            // dataGridView4
            // 
            this.dataGridView4.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView4.Location = new System.Drawing.Point(0, 0);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowTemplate.Height = 25;
            this.dataGridView4.Size = new System.Drawing.Size(1068, 359);
            this.dataGridView4.TabIndex = 0;
            // 
            // tabPageQC2
            // 
            this.tabPageQC2.Controls.Add(this.dataGridView3);
            this.tabPageQC2.Location = new System.Drawing.Point(4, 42);
            this.tabPageQC2.Name = "tabPageQC2";
            this.tabPageQC2.Size = new System.Drawing.Size(1068, 359);
            this.tabPageQC2.TabIndex = 3;
            this.tabPageQC2.Text = "QC2";
            // 
            // dataGridView3
            // 
            this.dataGridView3.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView3.Location = new System.Drawing.Point(0, 0);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.RowHeadersWidth = 51;
            this.dataGridView3.RowTemplate.Height = 29;
            this.dataGridView3.Size = new System.Drawing.Size(1068, 359);
            this.dataGridView3.TabIndex = 0;
            // 
            // tabPageQC1
            // 
            this.tabPageQC1.Controls.Add(this.dataGridView2);
            this.tabPageQC1.Location = new System.Drawing.Point(4, 42);
            this.tabPageQC1.Name = "tabPageQC1";
            this.tabPageQC1.Size = new System.Drawing.Size(1068, 359);
            this.tabPageQC1.TabIndex = 1;
            this.tabPageQC1.Text = "QC1";
            // 
            // dataGridView2
            // 
            this.dataGridView2.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView2.GridColor = System.Drawing.Color.Gray;
            this.dataGridView2.Location = new System.Drawing.Point(0, 0);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 29;
            this.dataGridView2.Size = new System.Drawing.Size(1068, 359);
            this.dataGridView2.TabIndex = 0;
            // 
            // tabPageCodes
            // 
            this.tabPageCodes.Controls.Add(this.dataGridView1);
            this.tabPageCodes.Location = new System.Drawing.Point(4, 42);
            this.tabPageCodes.Name = "tabPageCodes";
            this.tabPageCodes.Size = new System.Drawing.Size(1068, 359);
            this.tabPageCodes.TabIndex = 2;
            this.tabPageCodes.Text = "Codes";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.GridColor = System.Drawing.Color.White;
            this.dataGridView1.Location = new System.Drawing.Point(0, 0);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 29;
            this.dataGridView1.Size = new System.Drawing.Size(1068, 359);
            this.dataGridView1.TabIndex = 0;
            // 
            // tabPageRCC
            // 
            this.tabPageRCC.BackColor = System.Drawing.Color.Transparent;
            this.tabPageRCC.Controls.Add(this.richTextBox1);
            this.tabPageRCC.Location = new System.Drawing.Point(4, 42);
            this.tabPageRCC.Name = "tabPageRCC";
            this.tabPageRCC.Size = new System.Drawing.Size(1068, 359);
            this.tabPageRCC.TabIndex = 0;
            this.tabPageRCC.Text = "RCC";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1099, 761);
            this.Controls.Add(this.metroSetTabControl1);
            this.Controls.Add(this.metroSetButtonClose);
            this.Controls.Add(this.metroSetPanel1);
            this.Controls.Add(this.pictureBox1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "v1.0";
            this.ThemeAuthor = "Between";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.metroSetPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.metroSetTabControl1.ResumeLayout(false);
            this.tabPageQC3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.tabPageQC2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            this.tabPageQC1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.tabPageCodes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.tabPageRCC.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private MetroSet_UI.Controls.MetroSetTextBox textBoxExcelPath;
        private MetroSet_UI.Controls.MetroSetTextBox textBoxRccPath;
        private MetroSet_UI.Controls.MetroSetButton buttonLoadRccFile;
        private MetroSet_UI.Controls.MetroSetButton buttonLoadExcel;
        private MetroSet_UI.Controls.MetroSetRichTextBox richTextBox1;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private MetroSet_UI.Controls.MetroSetPanel metroSetPanel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroSet_UI.Controls.MetroSetButton metroSetButtonClose;
        private MetroSet_UI.Controls.MetroSetTabControl metroSetTabControl1;
        private System.Windows.Forms.TabPage tabPageRCC;
        private System.Windows.Forms.TabPage tabPageQC1;
        private MetroSet_UI.Controls.MetroSetButton metroSetButtonPDF;
        private System.Windows.Forms.TabPage tabPageCodes;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.TabPage tabPageQC2;
        private System.Windows.Forms.DataGridView dataGridView3;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel4;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel3;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel2;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel1;
        private MetroSet_UI.Controls.MetroSetTextBox metroSetTextBox2;
        private System.Windows.Forms.TabPage tabPageQC3;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private MetroSet_UI.Controls.MetroSetTextBox metroSetTextBox1;
        private MetroSet_UI.Controls.MetroSetLabel metroSetLabel5;
        private MetroSet_UI.Controls.MetroSetButton metroSetButton1;
        private MetroSet_UI.Controls.MetroSetTextBox metroSetTextBoxPath;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
    }
}

