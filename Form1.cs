﻿using DinkToPdf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RevealsGenomicsNetCore
{
    public partial class Form1 : MetroSet_UI.Forms.MetroSetForm
    {
        MainServices services;

        public Form1()
        {
            InitializeComponent();
            services = new MainServices();
            this.AllowResize = false;
            this.MaximumSize = new Size(this.Width,this.Height);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void buttonLoadExcel_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Excel Files  | *.csv; *.xlsx";
            openFileDialog1.Filter = "TXT Files  | *.txt";
            openFileDialog1.Multiselect = true;
            openFileDialog1.Title = "Open TXT Files";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                try
                {
                    textBoxExcelPath.Text = openFileDialog1.FileName;

                    //read EXCEL
                }
                catch (Exception ex)
                {
                    MetroSet_UI.Forms.MetroSetMessageBox.Show(this, "Error reading TXT file: \n" + ex.Message);
                }
            }
        }

        //LOAD RCC & QUALITY CONTROLS:
        private void buttonLoadRccFile_Click(object sender, EventArgs e)
        {
            //openFileDialog1.FileName = "*BT474*";
            openFileDialog1.Filter = "RCC files | *.rcc";
            openFileDialog1.Multiselect = true;
            openFileDialog1.Title = "Open RCC Files";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                string[] filesBT474 = Array.FindAll(openFileDialog1.FileNames, s => s.Contains("BT474"));
                string[] files = Array.FindAll(openFileDialog1.FileNames, s => !s.Contains("BT474"));
                //string file = openFileDialog1.FileName;
                try
                {
                    //textBoxRccPath.Text = openFileDialog1.FileName;
                    textBoxRccPath.Text = files[0];
                    metroSetTextBox1.Text = filesBT474[0];
                    metroSetTextBox2.Text = filesBT474[1];

                    string text = File.ReadAllText(files[0]);                    //Read RCC codes
                    var RCCResult = services.ReadXML(text);
                    var RCCBT474 = services.ReadXML(File.ReadAllText(filesBT474[0]));
                    var RCCBT474_2 = services.ReadXML(File.ReadAllText(filesBT474[1]));

                    richTextBox1.Text = text;

                    //services.ExportToTxtFile(RCCResult.codeSummaryList, new FileInfo("c:"));

                    
                    DataTable dtCodes = services.CreateDatatableFromList(RCCResult.codeSummaryList);
                    DataTable dtBT474 = services.CreateDatatableFromList(RCCResult.codeSummaryList);
                    DataTable dtBT474_2 = services.CreateDatatableFromList(RCCResult.codeSummaryList);


                    dataGridView1.DataSource = dtCodes;

                    DataTable dtWithoutNAs = services.UpdateCodesToHousekeeping(dtCodes.Copy()); //Filter 'GAPD','PUM1','ACTB','RPLP0', 'PSMC4' codes & update it CodeClass to Housekeeping
                   // metroSetTextBoxPath.Text = @"C:\Users\mmagallanes\Documents\RevealGenomics\NEW\SCRIPT_B20-006360-A-1\SCRIPT"; //for LOCAL TEST
                    DataTable dtOk = services.FilterHER2DX_FinalList(dtWithoutNAs, File.ReadAllText(metroSetTextBoxPath.Text));
                    
                    //QC1
                    DataTable dt1 = dtOk.Copy();
                    dataGridView2.DataSource = dt1;
                    var QC1Dto = services.qualityControl1(dt1);
                    dataGridView2.DataSource = QC1Dto.dt;
                    float QC1Calc = (float)QC1Dto.NAcount / (float)dt1.Rows.Count;
                    tabPageQC1.Text = "QC1 (" + QC1Calc + ")";
                    if (QC1Calc > 0.70)
                    {
                        MetroSet_UI.Forms.MetroSetMessageBox.Show(this, "Details: \n\nNA/Rows > 0.70", "Quality Control 1 Failed",  MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }


                    //QC2
                    DataTable dt2 = dtOk.Copy();
                    var QC2Dto = services.qualityControl2(dt2, dtBT474, dtBT474_2); // only use dtBt474 & dtBt474_2 files
                    dataGridView3.DataSource = QC2Dto.dt;
                    tabPageQC2.Text = "QC2 (" + QC2Dto.QC2value + ")";
                    if (QC2Dto.QC2value < 0 || QC2Dto.QC2value > 10)
                    {
                        MetroSet_UI.Forms.MetroSetMessageBox.Show(this, "Details: \n\n(mean(REF.sample)/REF.sample) = " + QC2Dto.QC2value, "Quality Control 2 Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                    //QC3
                    DataTable dt3 = dtOk.Copy();
                    var QC3Dto = services.qualityControl3(dt3);
                    dataGridView4.DataSource = QC3Dto.dt;
                    //tabPageQC2.Text = "QC3 (" + QC2Dto.QC2value + ")";
                    dataGridView4.AutoResizeColumns();

                }
                catch (Exception ex)
                {
                    MetroSet_UI.Forms.MetroSetMessageBox.Show(this, "Details: \n\n" + ex.Message, "Error reading RCC file", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void metroSetButtonClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void metroSetTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            dataGridView1.AutoResizeColumns();
            dataGridView2.AutoResizeColumns();
            dataGridView3.AutoResizeColumns();
            dataGridView4.AutoResizeColumns();

        }

        //GENERATE PDF:
        private void metroSetButtonPDF_Click(object sender, EventArgs e)
        {
            var converter = new BasicConverter(new PdfTools());

            var doc = new HtmlToPdfDocument()
            {
                GlobalSettings = {
                ColorMode = ColorMode.Color,
                Orientation = DinkToPdf.Orientation.Portrait,
                PaperSize = PaperKind.A4Plus,
            },
                Objects = {
                    new ObjectSettings()
                    {
                    PagesCount = true,
                    HtmlContent = @"<p>PDF Testing</p>", // CREATE HTML PAGE HERE!! 
                    WebSettings = { DefaultEncoding = "utf-8" },
                    HeaderSettings = { FontSize = 9, Right = "Page [page] of [toPage]", Line = true, Spacing = 2.812 }
                    }
    }
            };

            byte[] pdf = converter.Convert(doc);
            saveFileDialog1.Filter = "PDF|*.pdf";
            saveFileDialog1.FileName = "Result.pdf";

            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllBytes(saveFileDialog1.FileName, pdf); // Requires System.IO
            }


        }

        private void metroSetButton1_Click(object sender, EventArgs e)
        {
            //FOLDER PICK:
            //DialogResult result = folderBrowserDialog1.ShowDialog();
            //if (result == DialogResult.OK)
            //{
            //    metroSetTextBoxPath.Text = folderBrowserDialog1.SelectedPath;
            //}

            //FILE PICK:
            openFileDialog1.Filter = "TXT files | *.txt";
            openFileDialog1.Multiselect = true;
            openFileDialog1.Title = "Open TXT Files";
            DialogResult result = openFileDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                   metroSetTextBoxPath.Text = openFileDialog1.FileName;
            }



        }
    }
}
