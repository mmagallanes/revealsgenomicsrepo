﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevealsGenomicsNetCore
{
    public class CodeSummary
    {
        public String CodeClass;
        public String Name;
        public String Accession;
        public String Count;
    }

}
