﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RevealsGenomicsNetCore
{
    public class RCCDto
    {
        public Header header;
        public SampleAttributes sampleAttributes;
        public LaneAttributes laneAttributes;
        public List<CodeSummary> codeSummaryList;
    }
    public partial class Header
    {
        public String fileVersion;
        public String softwareVersion;

    }
    public partial class SampleAttributes
    {
        public String id;
        public String Owner;
        public String Comments;
        public String Date;
        public String GeneRLF;
        public String SystemAPF;

    }
    public partial class LaneAttributes
    {
        public String id;
        public String FovCount;
        public String FovCounted;
        public String ScannerId;
        public String StagePosition;
        public String BindingDensity;
        public String CartridgeID;
        public String CartridgeBarcode;

    }
    //public partial class CodeSummary
    //{
    //    public String CodeClass;
    //    public String Name;
    //    public String Accession;
    //    public String Count;
    //}
}
