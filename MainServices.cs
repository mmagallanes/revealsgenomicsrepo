﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace RevealsGenomicsNetCore
{
    public class MainServices
    {

        public RCCDto ReadXML(String fragment)
        {
            // XmlDocument doc = new XmlDocument();
            //XDocument doc2 = XDocument.Parse("<TempRoot>" + fragment + "</TempRoot>");
            XElement doc = XElement.Parse("<TempRoot>" + fragment + "</TempRoot>");
            var List = new List<string>();
            RCCDto CrrResult = new RCCDto();

            foreach (XElement item in doc.Descendants()) //For each TAG (header, sampleAtributtes, etc)
            {
                string[] innerRows = item.Value.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                switch (item.Name.LocalName)
                {
                    case "Header":
                        //int index = innerRows[0].LastIndexOf(",");
                        CrrResult.header = new Header { fileVersion = innerRows[0]?.Substring(innerRows[0].IndexOf(",") + 1), softwareVersion = innerRows[1]?.Substring(innerRows[1].IndexOf(",") + 1) };
                        break;
                    case "Sample_Attributes":
                        CrrResult.sampleAttributes = new SampleAttributes
                        {
                            id = innerRows[0]?.Substring(innerRows[0].IndexOf(",") + 1),
                            Owner = innerRows[1]?.Substring(innerRows[1].IndexOf(",") + 1),
                            Comments = innerRows[2]?.Substring(innerRows[2].IndexOf(",") + 1),
                            Date = innerRows[3]?.Substring(innerRows[3].IndexOf(",") + 1),
                            GeneRLF = innerRows[4]?.Substring(innerRows[4].IndexOf(",") + 1),
                            SystemAPF = innerRows[5]?.Substring(innerRows[5].IndexOf(",") + 1)
                        };
                        break;
                    case "Lane_Attributes":
                        CrrResult.laneAttributes = new LaneAttributes
                        {
                            id = innerRows[0]?.Substring(innerRows[0].IndexOf(",") + 1),
                            FovCount = innerRows[1]?.Substring(innerRows[1].IndexOf(",") + 1),
                            FovCounted = innerRows[2]?.Substring(innerRows[2].IndexOf(",") + 1),
                            ScannerId = innerRows[3]?.Substring(innerRows[3].IndexOf(",") + 1),
                            StagePosition = innerRows[4]?.Substring(innerRows[4].IndexOf(",") + 1),
                            BindingDensity = innerRows[5]?.Substring(innerRows[5].IndexOf(",") + 1),
                            CartridgeID = innerRows[6]?.Substring(innerRows[6].IndexOf(",") + 1),
                            CartridgeBarcode = innerRows[7]?.Substring(innerRows[7].IndexOf(",") + 1),
                        };
                        break;
                    case "Code_Summary":
                        CrrResult.codeSummaryList = new List<CodeSummary>();
                        int index = 0;
                        foreach (string row in innerRows)
                        {
                            if (index != 0) //skip first line (column's names)
                            {
                                var splitedRow = row.Split(',');
                                var cs = new CodeSummary() { CodeClass = splitedRow[0], Name = splitedRow[1], Accession = splitedRow[2], Count = splitedRow[3] };
                                CrrResult.codeSummaryList.Add(cs);
                            }
                            index++;

                        }

                        break;
                    default:
                        // code block
                        break;
                }
            }

            return CrrResult;
        }


        public DataTable CreateDatatableFromList(IEnumerable<CodeSummary> codes)
        {
            DataTable dt = new DataTable();
            dt.Clear();
            dt.Columns.Add("CodeClass");
            dt.Columns.Add("Name");
            dt.Columns.Add("Accession");
            dt.Columns.Add("Count");

            foreach (CodeSummary code in codes)
            {
                //metroSetRichTextBox1.Text += (code.CodeClass + "\t\t\t" + code.Name + "\t\t\t" + code.Accession + "\t\t\t" + code.Count + "\b");
                DataRow _row = dt.NewRow();
                _row["CodeClass"] = code.CodeClass;
                _row["Name"] = code.Name;
                _row["Accession"] = code.Accession;
                _row["Count"] = code.Count;
                dt.Rows.Add(_row);
            }
            return dt;
        }


        public DataTable UpdateCodesToHousekeeping(DataTable dt)
        {
            DataRow[] result = dt.Select("Name IN ('GAPD','PUM1','ACTB','RPLP0', 'PSMC4')"); //  find out only those record which are in...
            //result[0]["CodeClass"] = "Housekeeping";
            result.ToList<DataRow>().ForEach(r => r["CodeClass"] = "Housekeeping");
            dt.AcceptChanges();

            //order by:
            DataView dv = dt.DefaultView;
            dv.Sort = "CodeClass desc";
            dt = dv.ToTable();
            return dt;
            



            //DataView dv = new DataView(dt);
            //dv.RowFilter = "Name IN ('GAPD','PUM1','ACTB','RPLP0', 'PSMC4')";
            //var dtFiltered = dv.ToTable();
            //return dtFiltered;

        }

        public DataTable FilterHER2DX_FinalList(DataTable dt, string HER2DXFinalListDt)
        {
            string[] FilterNameValuesSplit = HER2DXFinalListDt.Split(Environment.NewLine.ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            List<string> list = new List<string>();
            foreach (string name in FilterNameValuesSplit)
            {
                var nameFixed = name.Split("\t")[0];
                list.Add(nameFixed);
            }
            String[] FilterNameValues = list.ToArray();

            var matches = dt.AsEnumerable()
                            .Where(dr => FilterNameValues.Any(l => dr.Field<string>("Name").Contains(l)))
                            .CopyToDataTable();

            return matches;
        }

            public QC1Dto qualityControl1(DataTable dt)  

        {
            int NAcount = 0;
            DataRow[] result = dt.Select("CodeClass = 'Endogenous'");
            foreach (DataRow row in result)
            {
                string countValueAsString = (string)row["Count"];
                int countValue;
                //endogenous with count < 20 set NA
                if (Int32.TryParse(countValueAsString, out countValue) && countValue < 20)
                {
                    NAcount++;
                    row.SetField("Count", "NA");
                };
            }
            dt.AcceptChanges();
            var dto = new QC1Dto() { dt = dt, NAcount = NAcount };
            return dto;
        }


        public void ExportToTxtFile(IEnumerable<CodeSummary> codes, FileInfo targetFile)
        {
            List<string> tempList = new List<string> { };
            foreach(CodeSummary code in codes)
            {
                tempList.Add(code.Accession + "\t" + code.CodeClass);

            }

            System.IO.File.WriteAllLines("SavedLists.txt", tempList);
        }


        //public QC1Dto qualityControl2_ToErase(DataTable dt, DataTable dtBT474, DataTable dtBT474_2)

        //{
  

        //    DataRow[] result = dt.Select("CodeClass = 'Endogenous'");
        //    //DataRow[] resultdtBT474 = dtBT474.Select("CodeClass = 'Endogenous'");
        //    //DataRow[] resultdtBT474_2 = dtBT474_2.Select("CodeClass = 'Endogenous'");

        //    dt.Columns.Add("BT474");
        //    dt.Columns.Add("BT474_2");
        //    dt.Columns.Add("GeometricMean", typeof(double));
        //    dt.AcceptChanges();

        //    foreach (DataRow row in result)
        //    {
        //        //string countValueAsString = (string)row["Count"];
        //        int index;
        //        var value1 = dtBT474.Select("CodeClass = 'Endogenous' AND  Name = '" + (string)row["Name"] + "'")[0].ItemArray[3].ToString(); //search the row in Samples (by name) and get its Count column value
        //        var value2 = dtBT474_2.Select("CodeClass = 'Endogenous' AND  Name = '" + (string)row["Name"] + "'")[0].ItemArray[3].ToString();
                
        //        row.SetField("BT474", value1);
        //        row.SetField("BT474_2", value2);
        //        row.SetField("GeometricMean", geometricMean(new int[] { Int32.Parse(value1), Int32.Parse(value2) },2));

        //    }
        //    dt.AcceptChanges();

        //    //float QC2value = Int32.Parse(dt.Compute("Avg(GeometricMean)", ""));

        //    var QC2value = dt.AsEnumerable()
        //                    .Average(r =>
        //                        r.Field<double?>(dt.Columns[6]));

        //    var dto = new QC1Dto() { dt = result.CopyToDataTable(), QC2value = QC2value };
        //    return dto;
        //}

        public QC1Dto qualityControl2(DataTable dt, DataTable dtBT474, DataTable dtBT474_2)

        {
            DataRow[] EndogenousBt474 = dtBT474.Select("CodeClass = 'Endogenous'");
            DataRow[] EndogenousBt474_2 = dtBT474_2.Select("CodeClass = 'Endogenous'");

            int[] arrray1 = EndogenousBt474.OfType<DataRow>().Select(k => Int32.Parse(k[3].ToString())).ToArray(); // count values
            int[] arrray2 = EndogenousBt474_2.OfType<DataRow>().Select(k => Int32.Parse(k[3].ToString())).ToArray(); // count values

            var GeoMean1 = geometricMean(arrray1, arrray1.Length);
            var GeoMean2 = geometricMean(arrray2, arrray2.Length);

            var mean = (GeoMean1 + GeoMean2) / 2;

            var resultTotalMeanQC2 = mean / GeoMean1; // mean dividid to same sample


            //DataRow[] result = dt.Select("CodeClass = 'Endogenous'");
            DataRow[] result = dtBT474.Select();
            DataTable DatatableResult = new DataTable();
            DatatableResult.Columns.Add("GeoMean BT474");
            DatatableResult.Columns.Add("GeoMean BT474_2");
            DatatableResult.Columns.Add("Mean");
            DatatableResult.Columns.Add("QC2 Result");
            DataRow row = DatatableResult.NewRow();
            row.SetField("GeoMean BT474", GeoMean1);
            row.SetField("GeoMean BT474_2", GeoMean2);
            row.SetField("Mean", mean);
            row.SetField("QC2 Result", resultTotalMeanQC2);

            DatatableResult.Rows.Add(row);

            DatatableResult.AcceptChanges();

            var dto = new QC1Dto() { dt = DatatableResult, QC2value = resultTotalMeanQC2 };
            return dto;
        }

        // function to calculate geometric mean
        // and return float value.s
        static float geometricMean(int[] arr, int n)
        {
            // declare sum variable and
            // initialize it to 1.
            float sum = 0;

            // Compute the sum of all the
            // elements in the array.
            for (int i = 0; i < n; i++)
                sum = sum + (float)Math.Log(arr[i]);

            // compute geometric mean through formula
            // antilog(((log(1) + log(2) + . . . + log(n))/n)
            // and return the value to main function.
            sum = sum / n;

            return (float)Math.Exp(sum);
            
            // function call:
            // Console.WriteLine(geometricMean(arr, n));
        }


        public QC1Dto qualityControl3(DataTable dt)

        {
            DataRow[] selectOnlyHouseKeeping = dt.Select("CodeClass = 'Housekeeping'");
            DataRow[] result = dt.Select();
            dt.Columns.Add("Count/GeoMean");
            dt.Columns.Add("Log2");
            int[] arrray1 = selectOnlyHouseKeeping.OfType<DataRow>().Select(k => Int32.Parse(k[3].ToString())).ToArray(); // count values of HouseKeeping genes
            var GeoMean1 = geometricMean(arrray1, arrray1.Length);

            foreach (DataRow row in result)
            {
                try 
                { 
                var countDividedbyGeoMean = Int32.Parse(row[3].ToString()) / GeoMean1;
                row.SetField("Count/GeoMean", countDividedbyGeoMean);
                double logBase2 = Math.Log2(countDividedbyGeoMean);
                row.SetField("Log2", logBase2);
                }
                catch (Exception ex)
                {
                    //It Won't set any field if can't parse
                }

            }
            dt.AcceptChanges();
            var dto = new QC1Dto() { dt = result.CopyToDataTable() };
            return dto;
        }


    }


    public class QC1Dto
    {
        public DataTable dt;
        public int? NAcount;
        public double? QC2value;

    }
}
